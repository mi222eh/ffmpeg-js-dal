import { FFMPEG } from './ffmpeg';
export declare function createConvertProcess(src: string, output: string): FFMPEG;
export declare function createCombineProcess(srcList: string[], output: string): FFMPEG;
